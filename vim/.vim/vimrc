"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File:   .vimrc
"         Main configuration file for (g)Vim

""" General vim options {{{1

let mapleader = " "
let g:mapleader = " "
set encoding=utf-8

" Load some sensible defaults
unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim

set autoread                    "Automatically update files that have been changed outside of Vim
set autoindent                  "Copy indent from current line when starting a new line
set tabstop=4 softtabstop=4     "The number of spaces a tab counts for
set shiftwidth=4                "Number of spaces used for each step of (auto)indent
set showmatch                   "Show matching brackets
set linebreak                   "Wrap long lines at a character in 'breakat' rather than at the last character that fits on the screen
set nowrap                      "Don't wrap long lines
set hlsearch                    "Highlight search pattern
set ignorecase                  "Case-insensitive search
set smartcase                   "Upper-case if search pattern contains uppercase
set shortmess-=S                "Show search count message when searching
set wildignorecase              "Ignore case for file completion
set lazyredraw                  "The screen will not be redrawn while executing macros
set cmdheight=2                 "Number of screen lines to use for the command-line.
set number                      "Show linenumbers
set splitbelow                  "New windows from horizontal split comes below
set splitright                  "New windows from vertical split opens to the right
set diffopt=vertical            "Split vertical when diffing
set foldmethod=marker           "Markers are used to specify folds.
set visualbell t_vb=            "No visualbell
set noswapfile                  "No backup
set nowritebackup               "No backup
set nobackup                    "No backup
set virtualedit=block           "Allow virtual editing in Visual block mode
set laststatus=2                "Status line also when only one window
set updatetime=750              "Updatetime, cursorhold etc
set list                        "List mode on
set listchars=tab:�\ ,trail:�,lead:�,extends:>,precedes:<
set signcolumn=yes

" Ignore these
set wildignore=*.avi,*.bmp,*.cdr,*.CGM,*.doc,*.eps,*.exe,*.gif,*.gni,*.JBK
set wildignore+=*.jpeg,*.jpg,*.lnk,*.mcd,*.MOD,*.mpeg,*.mpg,*.o,*.obj,*.pdf
set wildignore+=*.png,*.raf,*.SIN,*.xls,*.xlsm,*.xlsx,*.xmcd,*.xpm

" Make split windows equal size
function! ResizeWindows()
	let savetab = tabpagenr()
	tabdo wincmd =
		execute 'tabnext' savetab
endfunction

autocmd VimResized * call ResizeWindows()

" Trim whitespace when saving
fun! TrimWhitespace()
	let l:save = winsaveview()
	keeppatterns %s/\s\+$//e
	call winrestview(l:save)
endfun

autocmd BufWritePre * :call TrimWhitespace()

""" Plugins {{{1

" Packadd
packadd! matchit
packadd! comment
"packadd! nohlsearch

if has("win32")
	call plug#begin('~/vimfiles/plugged')
else
	call plug#begin('~/.vim/plugged')
endif

Plug 'sheerun/vim-polyglot'
Plug 'gruvbox-community/gruvbox'
Plug 'mbbill/undotree'
Plug 'vim/colorschemes'
Plug 'mechatroner/rainbow_csv'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'vim-airline/vim-airline'
Plug 'dense-analysis/ale'

call plug#end()

" Undotree
nnoremap <F5> :UndotreeToggle<cr>

" FZF
nnoremap <leader>sf :Files!<CR>
nnoremap <leader>sg :Rg!<CR>
nnoremap <C-p> :GFiles!<CR>

" Ale
let g:ale_linters = { 'python': ['flake8'] }
let g:ale_fixers = { 'python': ['black'] }


""" Key Mapping {{{1

" Clear highlight when pressing <Esc> in normal mode
nmap <Esc> :nohlsearch<CR>

" Count search matches
map <leader>/ :%s///gn<CR>

" Search next and previous at center of window
"map n nzzzv
"map N Nzzzv

" Move lines in visual mode
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Scroll one screen-line also when lines are wrapped
map j gj
map k gk

" Stay in column when joining lines
nnoremap J mzJ`z

" Quickfix window
nnoremap <C-k> :cprev<CR>zz
nnoremap <C-j> :cnext<CR>zz

" Toggle linewraps and listview
nnoremap <leader>w :set nowrap!<CR>
nnoremap <leader>l :set list!<CR>

" Toggle relative line number mode
function! NumberToggle()
	if (&relativenumber == 1)
		set norelativenumber
	else
		set relativenumber
	endif
endfunc

nnoremap <leader>rl :call NumberToggle()<CR>

" cd to directory of current file
map <leader>cd :cd %:h<cr>

" Edit vimrc
nmap <leader>v :e $MYVIMRC

" Bash-style command-line
cnoremap <C-A> <Home>
cnoremap <C-E> <End>
cnoremap <C-B> <Left>
cnoremap <C-F> <Right>
cnoremap <C-N> <Down>
cnoremap <C-P> <Up>

" Quickfix window
nnoremap <C-j> :cnext<CR>zz
nnoremap <C-k> :cprev<CR>zz

" Diff next/previous
nnoremap <leader>j ]c
nnoremap <leader>k [c

" Change behaviour of <S-K> to open Vim helpfile for word under cursor
"au BufReadPost * map K :exe ":help ".expand("<cword>")<CR>

" Clear buffer list
command! BufOnly execute "%bd | e# | echo 'Bufs Deleted'"
nnoremap <silent> <leader>bd :BufOnly<CR>

""" Colorscheme, Fonts & Cursorline {{{1

" Is it late?
function! IsLate()
	if has("win32")
		return (system('echo %time:~0,2%') >= 21 || system('echo %time:~0,2%') <= 7)
	else
		return (system('date +%H') >= 21 || system('date +%H') <= 7)
	endif
endfunction

" Colorscheme, time-of-day dependent
if IsLate()
	colorscheme habamax
	" set background=dark
else
	colorscheme gruvbox
	set background=dark
endif

" Font
if has("win32")
	set gfn=Cascadia_Code:h12,Hack:h11
else
	" set gfn=Monospace\ 12
	set gfn=Inconsolata\ Nerd\ Font\ 14
endif

""" Abbreviations. {{{1

" Short date and long date
iab ldate <c-r>=strftime("%d %B %Y - %H:%M:%S")<cr>
iab sdate <c-r>=strftime("%d %B %Y")<cr>

""" Window-size and GUI options {{{1

if has("gui_running")
	set guicursor&
	set guioptions-=T
	set guioptions-=m
	try
		set switchbuf=usetab
		set stal=1
	catch
	endtry

	if has("gui_win32")
		set lines=50
		set columns=130
		set visualbell t_vb=
		au GuiEnter * set visualbell t_vb=
	else
		set lines=50
		set columns=130
	endif

else
	" Make vim work better in terminal
	hi! Normal ctermbg=NONE guibg=NONE
	hi! NonText ctermbg=NONE guibg=NONE guifg=NONE ctermfg=NONE
endif

