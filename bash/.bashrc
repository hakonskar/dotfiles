#
# ~/.bashrc
#

## Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

## If not running interactively, don't do anything
[[ $- != *i* ]] && return

## don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000

## append to the history file, don't overwrite it
shopt -s histappend

## check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

## User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
    for rc in ~/.bashrc.d/*; do
        if [ -f "$rc" ]; then
            . "$rc"
        fi
    done
fi

## Use vim
export EDITOR=vim

## Ignore certain files for tab-completion
export FIGNORE=DS_Store:localized

## Path
PATH="$HOME/.local/bin:$HOME/bin:$PATH"
export PATH

## Starship prompt
export STARSHIP_CONFIG=~/.starship.toml
eval "$(starship init bash)"

## Set up fzf key bindings and fuzzy completion
eval "$(fzf --bash)"
#[ -f ~/.fzf.bash ] && source ~/.fzf.bash
