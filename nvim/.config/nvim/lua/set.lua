vim.opt.number = true
vim.opt.relativenumber = true

-- Enable mouse mode, can be useful for resizing splits for example!
vim.opt.mouse = 'a'

-- Don't show the mode, since it's already in the status line
--vim.opt.showmode = false

-- Indent
vim.opt.breakindent = true
vim.opt.smartindent = true

-- Tabs
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

-- Line wrap
vim.opt.wrap = false

-- Search
vim.opt.hlsearch = true
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.wildignorecase = true
vim.opt.inccommand = 'split'
vim.opt.shortmess:remove("S")

-- Sign/Color column
vim.opt.signcolumn = 'yes'
-- vim.opt.colorcolumn = "80"

-- Decrease update time
vim.opt.updatetime = 250

-- Decrease mapped sequence wait time
vim.opt.timeoutlen = 500

-- Window splits
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.diffopt = "vertical"

-- Backups
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undofile = false

vim.opt.termguicolors = true

-- Sets how neovim will display certain whitespace characters in the editor.
--  See `:help 'list'`
--  and `:help 'listchars'`
vim.opt.list = true
vim.opt.listchars = { tab = "» ", trail = "·", extends = ">", precedes = "<", lead = "·", nbsp = "␣" }

-- Minimal number of screen lines to keep above and below the cursor.
vim.opt.scrolloff = 7

-- Colorscheme
vim.cmd.colorscheme("tokyonight")
